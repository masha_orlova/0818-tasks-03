#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#define M 5
#define N 32

int main(){
	char str[M][N];
	int i=0,j=0,temp, rm[M],k;
	puts("Enter the strings");
	while (i<M){
		fgets(str[i], N, stdin);
		if (str[i][0]=='\n')
			break;
		i++;
		j++;
	}
	srand(time(0));
	printf("\n");
	for (i=0; i<j; i++)
		rm[i]=i;
	for (i=0; i<j; i++){
		k=rand()%j;
		temp=rm[i];
		rm[i]=rm[k];
		rm[k]=temp;
	}
	for(i=0;i<j;i++)
		printf("%s", str[rm[i]]);
	return 0;
}