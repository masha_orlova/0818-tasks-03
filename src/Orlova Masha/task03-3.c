
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define	M	6
#define	N	32

int main()
{
	char arr[M][N];
	int i=0,j=0,str;
	puts("Enter the strings:");
	while (i<M)
	{
		fgets(arr[i], N, stdin);
		if (arr[i][0]=='\n')
			break;
		i++;
		j++;
	}
	str = j;
	for (i=1;i<=N;i++)
		for (j=0;j<str;j++)
			if (strlen(arr[j]) == i)
				printf("%s", arr[j]);	
	return 0;
}