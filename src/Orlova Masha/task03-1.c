#include <stdio.h>
#include <string.h>
#define N 128
int main(){
	char strarr[N][N];
	int i = 0;
	printf("Enter the strings:\n");
	for (i; i < N; i++){
		fgets(strarr[i], N, stdin);
		if (strlen(strarr[i]) == 1){
			i--;
			break;
		}
	}
	printf("\n strings in reverse order:\n");
	for (i; i >=0; i--){
		for (int j = strlen(strarr[i]); j >= 0; j--){
			printf("%c", strarr[i][j]);
		}
	}
	return 0;
}